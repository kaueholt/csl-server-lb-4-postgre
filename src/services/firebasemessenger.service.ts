import {bind, /* inject, */ BindingScope, Provider} from '@loopback/core';

/*
 * Fix the service type. Possible options can be:
 * - import {Firebasemessenger} from 'your-module';
 * - export type Firebasemessenger = string;
 * - export interface Firebasemessenger {}
 */
export type Firebasemessenger = unknown;

@bind({scope: BindingScope.TRANSIENT})
export class FirebasemessengerProvider implements Provider<Firebasemessenger> {
  constructor(/* Add @inject to inject parameters */) {}

  value() {
    // Add your implementation here
    throw new Error('To be implemented');
  }
}
