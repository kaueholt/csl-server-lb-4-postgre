import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceUserProfile} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceUserProfile,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-user-profiles',
};
module.exports = config;
