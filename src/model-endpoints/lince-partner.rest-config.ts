import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LincePartner} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LincePartner,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-partners',
};
module.exports = config;
