import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceTip} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceTip,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-tips',
};
module.exports = config;
