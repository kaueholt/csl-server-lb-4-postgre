import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {EquipmentModels} from '../models';

const config: ModelCrudRestApiConfig = {
  model: EquipmentModels,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/equipment-models',
};
module.exports = config;
