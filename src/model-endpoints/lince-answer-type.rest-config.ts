import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceAnswerType} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceAnswerType,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-answer-types',
};
module.exports = config;
