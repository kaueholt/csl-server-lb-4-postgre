import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {EquipmentPatrimonies} from '../models';

const config: ModelCrudRestApiConfig = {
  model: EquipmentPatrimonies,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/equipment-patrimonies',
};
module.exports = config;
