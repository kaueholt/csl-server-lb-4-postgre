import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceProfile} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceProfile,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-profiles',
};
module.exports = config;
