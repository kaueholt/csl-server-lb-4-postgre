import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceWebhook} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceWebhook,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-webhooks-rest-crud',
};
module.exports = config;
