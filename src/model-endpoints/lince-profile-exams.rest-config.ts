import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceProfileExams} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceProfileExams,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-profile-exams',
};
module.exports = config;
