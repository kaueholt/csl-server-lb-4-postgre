import {ModelCrudRestApiConfig} from '@loopback/rest-crud';
import {LinceHilabWebhook} from '../models';

const config: ModelCrudRestApiConfig = {
  model: LinceHilabWebhook,
  pattern: 'CrudRest',
  dataSource: 'postgre',
  basePath: '/lince-hilab-webhooks',
};
module.exports = config;
