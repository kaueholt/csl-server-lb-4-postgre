import {authenticate} from '@loopback/authentication';
import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where
} from '@loopback/repository';
import {
  del, get,
  getModelSchemaRef, param,


  patch, post,




  put,

  requestBody
} from '@loopback/rest';
import {LinceWebhook} from '../models';
import {LinceWebhookRepository} from '../repositories';

@authenticate('jwt')
export class WebhookController {
  constructor(
    @repository(LinceWebhookRepository)
    public linceWebhookRepository: LinceWebhookRepository,
  ) {}
  @authenticate('jwt')
  @post('/lince-webhooks', {
    responses: {
      '200': {
        description: 'LinceWebhook model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceWebhook)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceWebhook, {
            title: 'NewLinceWebhook',
            exclude: ['id'],
          }),
        },
      },
    })
    linceWebhook: Omit<LinceWebhook, 'id'>,
  ): Promise<LinceWebhook> {
    return this.linceWebhookRepository.create(linceWebhook);
  }

  @get('/lince-webhooks/count', {
    responses: {
      '200': {
        description: 'LinceWebhook model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(LinceWebhook) where?: Where<LinceWebhook>,
  ): Promise<Count> {
    return this.linceWebhookRepository.count(where);
  }

  @get('/lince-webhooks', {
    responses: {
      '200': {
        description: 'Array of LinceWebhook model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(LinceWebhook, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(LinceWebhook) filter?: Filter<LinceWebhook>,
  ): Promise<LinceWebhook[]> {
    return this.linceWebhookRepository.find(filter);
  }

  @patch('/lince-webhooks', {
    responses: {
      '200': {
        description: 'LinceWebhook PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceWebhook, {partial: true}),
        },
      },
    })
    linceWebhook: LinceWebhook,
    @param.where(LinceWebhook) where?: Where<LinceWebhook>,
  ): Promise<Count> {
    return this.linceWebhookRepository.updateAll(linceWebhook, where);
  }

  @get('/lince-webhooks/{id}', {
    responses: {
      '200': {
        description: 'LinceWebhook model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LinceWebhook, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.string('id') id: string,
    @param.filter(LinceWebhook, {exclude: 'where'}) filter?: FilterExcludingWhere<LinceWebhook>
  ): Promise<LinceWebhook> {
    return this.linceWebhookRepository.findById(id, filter);
  }

  @patch('/lince-webhooks/{id}', {
    responses: {
      '204': {
        description: 'LinceWebhook PATCH success',
      },
    },
  })
  async updateById(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceWebhook, {partial: true}),
        },
      },
    })
    linceWebhook: LinceWebhook,
  ): Promise<void> {
    await this.linceWebhookRepository.updateById(id, linceWebhook);
  }

  @put('/lince-webhooks/{id}', {
    responses: {
      '204': {
        description: 'LinceWebhook PUT success',
      },
    },
  })
  async replaceById(
    @param.path.string('id') id: string,
    @requestBody() linceWebhook: LinceWebhook,
  ): Promise<void> {
    await this.linceWebhookRepository.replaceById(id, linceWebhook);
  }

  @del('/lince-webhooks/{id}', {
    responses: {
      '204': {
        description: 'LinceWebhook DELETE success',
      },
    },
  })
  async deleteById(@param.path.string('id') id: string): Promise<void> {
    await this.linceWebhookRepository.deleteById(id);
  }
}
