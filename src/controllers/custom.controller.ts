// Uncomment these imports to begin using these cool features!

// import {inject} from '@loopback/core';

import {repository} from '@loopback/repository';
import {
  get,

  getModelSchemaRef, param
} from '@loopback/rest';
import {LinceProfile, LinceUser, Users} from '../models';
import {LinceProfileRepository} from '../repositories';

export class CustomController {
  constructor(
    @repository(LinceProfileRepository)
    public linceProfileRepository: LinceProfileRepository,
  ) {}
  @get('/resposta/dica/{dica}/usuario/{usuario}', {
    responses: {
      '200': {
        description: 'Verifica a resposta do usuario com id {usuario} para a dica com id {dica}',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LinceProfile, {includeRelations: true}),
          },
        },
      },
    },
  })
  async getAnswerFromUser(@param.path.string('dica') dica: string, @param.path.string('usuario') usuario: string): Promise<LinceProfile[]> {
    let sqlStmt = "SELECT lince_tip.id as lince_tip_id, " +
      "lince_tip.titulo, " +
      "lince_tip.descricao, " +
      "lince_tip.link, " +
      "lince_tip.status, " +
      "lince_tip_user.id as lince_tip_user_id, " +
      "lince_tip_user.data_envio, " +
      "lince_tip_user.media, " +
      "lince_tip_question.id as lince_tip_question_id, " +
      "lince_tip_question.pergunta, " +
      "lince_tip_question.peso, " +
      "lince_tip_question.tipo_resposta, " +
      "lince_tip_question.resposta as resposta_esperada, " +
      "lince_tip_user_answer.id as lince_tip_user_answer_id, " +
      "lince_tip_user_answer.resposta as resposta_usuario " +
      "FROM lince_tip " +
      "LEFT JOIN lince_tip_user " +
      "ON lince_tip_user.dica_id = lince_tip.id " +
      "LEFT JOIN lince_tip_question " +
      "ON lince_tip_question.dica_id = lince_tip.id " +
      "LEFT JOIN lince_tip_user_answer " +
      "ON lince_tip_user_answer.usuariodica_id = lince_tip_user.id " +
      "AND lince_tip_user_answer.dicapergunta_id = lince_tip_question.id " +
      "WHERE lince_tip.id = '" + dica + "' " +
      "AND lince_tip_user.lince_usuario_id = '" + usuario + "' "
    return this.linceProfileRepository.dataSource.execute(sqlStmt);
  }

  @get('/linkUsers', {
    responses: {
      '200': {
        description: 'Recupera informações do usuário Lince no banco SESI. (O usuário-pai é do SESI, e portanto suas informações pessoais devem ser recuperadas a partir do SESI.)',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(Users, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async linkUsers(): Promise<LinceUser[]> {
    let sqlStmt = "SELECT lince_user.id as LINCE_id, " +
      "users.id as SESI_id, users.avatar as SESI_avatar, users.name as SESI_name, users.cpf as SESI_cpf, " +
      "users.password as SESI_password, users.token as SESI_token, users.email as SESI_email, users.date_birth as SESI_date_birth, " +
      "users.status as SESI_status, users.role as SESI_role " +
      "FROM lince_user LEFT JOIN users ON lince_user.sesi_usuario_id = users.id"
    return this.linceProfileRepository.dataSource.execute(sqlStmt);
  }

  @get('/perfis/usuario/{id}', {
    responses: {
      '200': {
        description: 'Recupera informações dos perfís do usuário LINCE',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LinceProfile, {includeRelations: true}),
          },
        },
      },
    },
  })
  async getUserProfiles(@param.path.string('id') id: string): Promise<LinceProfile[]> {
    let sqlStmt = "SELECT lince_user_profile.id as id_lince_user_profile, lince_profile.id as id_perfil, lince_profile.descricao, lince_user_profile.origem_cadastro " +
      "FROM lince_user_profile " +
      "LEFT JOIN lince_profile ON lince_user_profile.perfil_id = lince_profile.id " +
      "LEFT JOIN lince_user ON lince_user_profile.lince_usuario_id = lince_user.id " +
      "WHERE lince_user.status > 0  " +
      "AND lince_user.id = '" + id + "'"
    return this.linceProfileRepository.dataSource.execute(sqlStmt);
  }

  @get('/perfis/diferentes/usuario/{id}', {
    responses: {
      '200': {
        description: 'Verifica os novos perfís que o usuário pode escolher',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LinceProfile, {includeRelations: true}),
          },
        },
      },
    },
  })
  async getAvailableProfilesToTheUser(@param.path.string('id') id: string): Promise<LinceProfile[]> {
    let sqlStmt = "SELECT lince_profile.id, lince_profile.descricao " +
      "FROM lince_profile " +
      "WHERE lince_profile.id NOT IN (	 " +
      "SELECT lince_user_profile.perfil_id " +
      "FROM lince_user_profile " +
      "WHERE lince_user_profile.lince_usuario_id = '" + id + "' " +
      ") "
    return this.linceProfileRepository.dataSource.execute(sqlStmt);
  }

}
