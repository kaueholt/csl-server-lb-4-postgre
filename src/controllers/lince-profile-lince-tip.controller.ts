import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  LinceProfile,
  LinceTip,
} from '../models';
import {LinceProfileRepository} from '../repositories';

export class LinceProfileLinceTipController {
  constructor(
    @repository(LinceProfileRepository) protected linceProfileRepository: LinceProfileRepository,
  ) { }

  @get('/lince-profiles/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'Array of LinceProfile has many LinceTip',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceTip)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceTip>,
  ): Promise<LinceTip[]> {
    return this.linceProfileRepository.dica(id).find(filter);
  }

  @post('/lince-profiles/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'LinceProfile model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceTip)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceProfile.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTip, {
            title: 'NewLinceTipInLinceProfile',
            exclude: ['id'],
            optional: ['perfil_id']
          }),
        },
      },
    }) linceTip: Omit<LinceTip, 'id'>,
  ): Promise<LinceTip> {
    return this.linceProfileRepository.dica(id).create(linceTip);
  }

  @patch('/lince-profiles/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'LinceProfile.LinceTip PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTip, {partial: true}),
        },
      },
    })
    linceTip: Partial<LinceTip>,
    @param.query.object('where', getWhereSchemaFor(LinceTip)) where?: Where<LinceTip>,
  ): Promise<Count> {
    return this.linceProfileRepository.dica(id).patch(linceTip, where);
  }

  @del('/lince-profiles/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'LinceProfile.LinceTip DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceTip)) where?: Where<LinceTip>,
  ): Promise<Count> {
    return this.linceProfileRepository.dica(id).delete(where);
  }
}
