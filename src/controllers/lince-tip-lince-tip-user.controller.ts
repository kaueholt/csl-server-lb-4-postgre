import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  LinceTip,
  LinceTipUser,
} from '../models';
import {LinceTipRepository} from '../repositories';

export class LinceTipLinceTipUserController {
  constructor(
    @repository(LinceTipRepository) protected linceTipRepository: LinceTipRepository,
  ) { }

  @get('/lince-tips/{id}/lince-tip-users', {
    responses: {
      '200': {
        description: 'Array of LinceTip has many LinceTipUser',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceTipUser)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceTipUser>,
  ): Promise<LinceTipUser[]> {
    return this.linceTipRepository.usuarioDica(id).find(filter);
  }

  @post('/lince-tips/{id}/lince-tip-users', {
    responses: {
      '200': {
        description: 'LinceTip model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceTipUser)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceTip.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipUser, {
            title: 'NewLinceTipUserInLinceTip',
            exclude: ['id'],
            optional: ['dicaId']
          }),
        },
      },
    }) linceTipUser: Omit<LinceTipUser, 'id'>,
  ): Promise<LinceTipUser> {
    return this.linceTipRepository.usuarioDica(id).create(linceTipUser);
  }

  @patch('/lince-tips/{id}/lince-tip-users', {
    responses: {
      '200': {
        description: 'LinceTip.LinceTipUser PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipUser, {partial: true}),
        },
      },
    })
    linceTipUser: Partial<LinceTipUser>,
    @param.query.object('where', getWhereSchemaFor(LinceTipUser)) where?: Where<LinceTipUser>,
  ): Promise<Count> {
    return this.linceTipRepository.usuarioDica(id).patch(linceTipUser, where);
  }

  @del('/lince-tips/{id}/lince-tip-users', {
    responses: {
      '200': {
        description: 'LinceTip.LinceTipUser DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceTipUser)) where?: Where<LinceTipUser>,
  ): Promise<Count> {
    return this.linceTipRepository.usuarioDica(id).delete(where);
  }
}
