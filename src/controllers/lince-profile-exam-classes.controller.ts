import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
  import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
LinceProfile,
LinceProfileExams,
ExamClasses,
} from '../models';
import {LinceProfileRepository} from '../repositories';

export class LinceProfileExamClassesController {
  constructor(
    @repository(LinceProfileRepository) protected linceProfileRepository: LinceProfileRepository,
  ) { }

  @get('/lince-profiles/{id}/exam-classes', {
    responses: {
      '200': {
        description: 'Array of LinceProfile has many ExamClasses through LinceProfileExams',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(ExamClasses)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<ExamClasses>,
  ): Promise<ExamClasses[]> {
    return this.linceProfileRepository.exame(id).find(filter);
  }

  @post('/lince-profiles/{id}/exam-classes', {
    responses: {
      '200': {
        description: 'create a ExamClasses model instance',
        content: {'application/json': {schema: getModelSchemaRef(ExamClasses)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceProfile.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExamClasses, {
            title: 'NewExamClassesInLinceProfile',
            exclude: ['id'],
          }),
        },
      },
    }) examClasses: Omit<ExamClasses, 'id'>,
  ): Promise<ExamClasses> {
    return this.linceProfileRepository.exame(id).create(examClasses);
  }

  @patch('/lince-profiles/{id}/exam-classes', {
    responses: {
      '200': {
        description: 'LinceProfile.ExamClasses PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ExamClasses, {partial: true}),
        },
      },
    })
    examClasses: Partial<ExamClasses>,
    @param.query.object('where', getWhereSchemaFor(ExamClasses)) where?: Where<ExamClasses>,
  ): Promise<Count> {
    return this.linceProfileRepository.exame(id).patch(examClasses, where);
  }

  @del('/lince-profiles/{id}/exam-classes', {
    responses: {
      '200': {
        description: 'LinceProfile.ExamClasses DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(ExamClasses)) where?: Where<ExamClasses>,
  ): Promise<Count> {
    return this.linceProfileRepository.exame(id).delete(where);
  }
}
