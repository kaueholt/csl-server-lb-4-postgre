import {
  repository,
} from '@loopback/repository';
import {
  param,
  get,
  getModelSchemaRef,
} from '@loopback/rest';
import {
  LinceTip,
  LinceProfile,
} from '../models';
import {LinceTipRepository} from '../repositories';

export class LinceTipLinceProfileController {
  constructor(
    @repository(LinceTipRepository)
    public linceTipRepository: LinceTipRepository,
  ) { }

  @get('/lince-tips/{id}/lince-profile', {
    responses: {
      '200': {
        description: 'LinceProfile belonging to LinceTip',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceProfile)},
          },
        },
      },
    },
  })
  async getLinceProfile(
    @param.path.string('id') id: typeof LinceTip.prototype.id,
  ): Promise<LinceProfile> {
    return this.linceTipRepository.perfil(id);
  }
}
