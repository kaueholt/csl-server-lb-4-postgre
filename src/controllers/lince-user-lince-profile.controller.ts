import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody
} from '@loopback/rest';
import {LinceProfile, LinceUser} from '../models';
import {LinceUserRepository} from '../repositories';

export class LinceUserLinceProfileController {
  constructor(
    @repository(LinceUserRepository) protected linceUserRepository: LinceUserRepository,
  ) {}

  @get('/lince-users/{id}/lince-profiles', {
    responses: {
      '200': {
        description: 'Array of LinceUser has many LinceProfile through LinceUserProfile',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceProfile)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceProfile>,
  ): Promise<LinceProfile[]> {
    return this.linceUserRepository.perfil(id).find(filter);
  }

  @post('/lince-users/{id}/lince-profiles', {
    responses: {
      '200': {
        description: 'create a LinceProfile model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceProfile)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceUser.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceProfile, {
            title: 'NewLinceProfileInLinceUser',
            exclude: ['id'],
          }),
        },
      },
    }) linceProfile: Omit<LinceProfile, 'id'>,
  ): Promise<LinceProfile> {
    return this.linceUserRepository.perfil(id).create(linceProfile);
  }

  @patch('/lince-users/{id}/lince-profiles', {
    responses: {
      '200': {
        description: 'LinceUser.LinceProfile PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceProfile, {partial: true}),
        },
      },
    })
    linceProfile: Partial<LinceProfile>,
    @param.query.object('where', getWhereSchemaFor(LinceProfile)) where?: Where<LinceProfile>,
  ): Promise<Count> {
    return this.linceUserRepository.perfil(id).patch(linceProfile, where);
  }

  @del('/lince-users/{id}/lince-profiles', {
    responses: {
      '200': {
        description: 'LinceUser.LinceProfile DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceProfile)) where?: Where<LinceProfile>,
  ): Promise<Count> {
    return this.linceUserRepository.perfil(id).delete(where);
  }
}
