import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  LinceTip,
  LinceTipQuestion,
} from '../models';
import {LinceTipRepository} from '../repositories';

export class LinceTipLinceTipQuestionController {
  constructor(
    @repository(LinceTipRepository) protected linceTipRepository: LinceTipRepository,
  ) { }

  @get('/lince-tips/{id}/lince-tip-questions', {
    responses: {
      '200': {
        description: 'Array of LinceTip has many LinceTipQuestion',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceTipQuestion)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceTipQuestion>,
  ): Promise<LinceTipQuestion[]> {
    return this.linceTipRepository.dicaPergunta(id).find(filter);
  }

  @post('/lince-tips/{id}/lince-tip-questions', {
    responses: {
      '200': {
        description: 'LinceTip model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceTipQuestion)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceTip.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipQuestion, {
            title: 'NewLinceTipQuestionInLinceTip',
            exclude: ['id'],
            optional: ['dicaId']
          }),
        },
      },
    }) linceTipQuestion: Omit<LinceTipQuestion, 'id'>,
  ): Promise<LinceTipQuestion> {
    return this.linceTipRepository.dicaPergunta(id).create(linceTipQuestion);
  }

  @patch('/lince-tips/{id}/lince-tip-questions', {
    responses: {
      '200': {
        description: 'LinceTip.LinceTipQuestion PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipQuestion, {partial: true}),
        },
      },
    })
    linceTipQuestion: Partial<LinceTipQuestion>,
    @param.query.object('where', getWhereSchemaFor(LinceTipQuestion)) where?: Where<LinceTipQuestion>,
  ): Promise<Count> {
    return this.linceTipRepository.dicaPergunta(id).patch(linceTipQuestion, where);
  }

  @del('/lince-tips/{id}/lince-tip-questions', {
    responses: {
      '200': {
        description: 'LinceTip.LinceTipQuestion DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceTipQuestion)) where?: Where<LinceTipQuestion>,
  ): Promise<Count> {
    return this.linceTipRepository.dicaPergunta(id).delete(where);
  }
}
