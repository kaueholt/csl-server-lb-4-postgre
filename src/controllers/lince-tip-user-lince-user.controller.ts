import {repository} from '@loopback/repository';
import {
  get,
  getModelSchemaRef, param
} from '@loopback/rest';
import {
  LinceTipUser,
  LinceUser
} from '../models';
import {LinceTipUserRepository} from '../repositories';

// @authenticate('jwt')

export class LinceTipUserLinceUserController {
  constructor(
    @repository(LinceTipUserRepository)
    public linceTipUserRepository: LinceTipUserRepository,
  ) {}

  @get('/lince-tip-users/{id}/lince-user', {
    responses: {
      '200': {
        description: 'LinceUser belonging to LinceTipUser',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceUser)},
          },
        },
      },
    },
  })
  async getLinceUser(
    @param.path.string('id') id: typeof LinceTipUser.prototype.id,
  ): Promise<LinceUser> {
    return this.linceTipUserRepository.linceUsuario(id);
  }
}
