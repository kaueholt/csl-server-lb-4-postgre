import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  LinceTipQuestion,
  LinceTipUserAnswer,
} from '../models';
import {LinceTipQuestionRepository} from '../repositories';

export class LinceTipQuestionLinceTipUserAnswerController {
  constructor(
    @repository(LinceTipQuestionRepository) protected linceTipQuestionRepository: LinceTipQuestionRepository,
  ) { }

  @get('/lince-tip-questions/{id}/lince-tip-user-answers', {
    responses: {
      '200': {
        description: 'Array of LinceTipQuestion has many LinceTipUserAnswer',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceTipUserAnswer)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceTipUserAnswer>,
  ): Promise<LinceTipUserAnswer[]> {
    return this.linceTipQuestionRepository.usuarioResposta(id).find(filter);
  }

  @post('/lince-tip-questions/{id}/lince-tip-user-answers', {
    responses: {
      '200': {
        description: 'LinceTipQuestion model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceTipUserAnswer)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceTipQuestion.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipUserAnswer, {
            title: 'NewLinceTipUserAnswerInLinceTipQuestion',
            exclude: ['id'],
            optional: ['dicaPerguntaId']
          }),
        },
      },
    }) linceTipUserAnswer: Omit<LinceTipUserAnswer, 'id'>,
  ): Promise<LinceTipUserAnswer> {
    return this.linceTipQuestionRepository.usuarioResposta(id).create(linceTipUserAnswer);
  }

  @patch('/lince-tip-questions/{id}/lince-tip-user-answers', {
    responses: {
      '200': {
        description: 'LinceTipQuestion.LinceTipUserAnswer PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipUserAnswer, {partial: true}),
        },
      },
    })
    linceTipUserAnswer: Partial<LinceTipUserAnswer>,
    @param.query.object('where', getWhereSchemaFor(LinceTipUserAnswer)) where?: Where<LinceTipUserAnswer>,
  ): Promise<Count> {
    return this.linceTipQuestionRepository.usuarioResposta(id).patch(linceTipUserAnswer, where);
  }

  @del('/lince-tip-questions/{id}/lince-tip-user-answers', {
    responses: {
      '200': {
        description: 'LinceTipQuestion.LinceTipUserAnswer DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceTipUserAnswer)) where?: Where<LinceTipUserAnswer>,
  ): Promise<Count> {
    return this.linceTipQuestionRepository.usuarioResposta(id).delete(where);
  }
}
