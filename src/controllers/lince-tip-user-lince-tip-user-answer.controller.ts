import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
  LinceTipUser,
  LinceTipUserAnswer,
} from '../models';
import {LinceTipUserRepository} from '../repositories';

export class LinceTipUserLinceTipUserAnswerController {
  constructor(
    @repository(LinceTipUserRepository) protected linceTipUserRepository: LinceTipUserRepository,
  ) { }

  @get('/lince-tip-users/{id}/lince-tip-user-answer', {
    responses: {
      '200': {
        description: 'LinceTipUser has one LinceTipUserAnswer',
        content: {
          'application/json': {
            schema: getModelSchemaRef(LinceTipUserAnswer),
          },
        },
      },
    },
  })
  async get(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceTipUserAnswer>,
  ): Promise<LinceTipUserAnswer> {
    return this.linceTipUserRepository.usuarioResposta(id).get(filter);
  }

  @post('/lince-tip-users/{id}/lince-tip-user-answer', {
    responses: {
      '200': {
        description: 'LinceTipUser model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceTipUserAnswer)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceTipUser.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipUserAnswer, {
            title: 'NewLinceTipUserAnswerInLinceTipUser',
            exclude: ['id'],
            optional: ['usuarioDicaId']
          }),
        },
      },
    }) linceTipUserAnswer: Omit<LinceTipUserAnswer, 'id'>,
  ): Promise<LinceTipUserAnswer> {
    return this.linceTipUserRepository.usuarioResposta(id).create(linceTipUserAnswer);
  }

  @patch('/lince-tip-users/{id}/lince-tip-user-answer', {
    responses: {
      '200': {
        description: 'LinceTipUser.LinceTipUserAnswer PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTipUserAnswer, {partial: true}),
        },
      },
    })
    linceTipUserAnswer: Partial<LinceTipUserAnswer>,
    @param.query.object('where', getWhereSchemaFor(LinceTipUserAnswer)) where?: Where<LinceTipUserAnswer>,
  ): Promise<Count> {
    return this.linceTipUserRepository.usuarioResposta(id).patch(linceTipUserAnswer, where);
  }

  @del('/lince-tip-users/{id}/lince-tip-user-answer', {
    responses: {
      '200': {
        description: 'LinceTipUser.LinceTipUserAnswer DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceTipUserAnswer)) where?: Where<LinceTipUserAnswer>,
  ): Promise<Count> {
    return this.linceTipUserRepository.usuarioResposta(id).delete(where);
  }
}
