import {
  Count,
  CountSchema,
  Filter,
  repository,
  Where,
} from '@loopback/repository';
  import {
  del,
  get,
  getModelSchemaRef,
  getWhereSchemaFor,
  param,
  patch,
  post,
  requestBody,
} from '@loopback/rest';
import {
LinceUser,
LinceTipUser,
LinceTip,
} from '../models';
import {LinceUserRepository} from '../repositories';

export class LinceUserLinceTipController {
  constructor(
    @repository(LinceUserRepository) protected linceUserRepository: LinceUserRepository,
  ) { }

  @get('/lince-users/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'Array of LinceUser has many LinceTip through LinceTipUser',
        content: {
          'application/json': {
            schema: {type: 'array', items: getModelSchemaRef(LinceTip)},
          },
        },
      },
    },
  })
  async find(
    @param.path.string('id') id: string,
    @param.query.object('filter') filter?: Filter<LinceTip>,
  ): Promise<LinceTip[]> {
    return this.linceUserRepository.dica(id).find(filter);
  }

  @post('/lince-users/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'create a LinceTip model instance',
        content: {'application/json': {schema: getModelSchemaRef(LinceTip)}},
      },
    },
  })
  async create(
    @param.path.string('id') id: typeof LinceUser.prototype.id,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTip, {
            title: 'NewLinceTipInLinceUser',
            exclude: ['id'],
          }),
        },
      },
    }) linceTip: Omit<LinceTip, 'id'>,
  ): Promise<LinceTip> {
    return this.linceUserRepository.dica(id).create(linceTip);
  }

  @patch('/lince-users/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'LinceUser.LinceTip PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async patch(
    @param.path.string('id') id: string,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(LinceTip, {partial: true}),
        },
      },
    })
    linceTip: Partial<LinceTip>,
    @param.query.object('where', getWhereSchemaFor(LinceTip)) where?: Where<LinceTip>,
  ): Promise<Count> {
    return this.linceUserRepository.dica(id).patch(linceTip, where);
  }

  @del('/lince-users/{id}/lince-tips', {
    responses: {
      '200': {
        description: 'LinceUser.LinceTip DELETE success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async delete(
    @param.path.string('id') id: string,
    @param.query.object('where', getWhereSchemaFor(LinceTip)) where?: Where<LinceTip>,
  ): Promise<Count> {
    return this.linceUserRepository.dica(id).delete(where);
  }
}
