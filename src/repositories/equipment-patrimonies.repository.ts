import {DefaultCrudRepository} from '@loopback/repository';
import {EquipmentPatrimonies, EquipmentPatrimoniesRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EquipmentPatrimoniesRepository extends DefaultCrudRepository<
  EquipmentPatrimonies,
  typeof EquipmentPatrimonies.prototype.id,
  EquipmentPatrimoniesRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(EquipmentPatrimonies, dataSource);
  }
}
