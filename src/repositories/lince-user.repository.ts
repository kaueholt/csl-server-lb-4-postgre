import {Getter, inject} from '@loopback/core';
import {DefaultCrudRepository, HasManyThroughRepositoryFactory, repository} from '@loopback/repository';
import {PostgreDataSource} from '../datasources';
import {LinceProfile, LinceTip, LinceTipUser, LinceUser, LinceUserProfile, LinceUserRelations} from '../models';
import {LinceProfileRepository} from './lince-profile.repository';
import {LinceTipUserRepository} from './lince-tip-user.repository';
import {LinceTipRepository} from './lince-tip.repository';
import {LinceUserProfileRepository} from './lince-user-profile.repository';

export class LinceUserRepository extends DefaultCrudRepository<
  LinceUser,
  typeof LinceUser.prototype.id,
  LinceUserRelations
  > {

  public readonly perfil: HasManyThroughRepositoryFactory<LinceProfile, typeof LinceProfile.prototype.id,
    LinceUserProfile,
    typeof LinceUser.prototype.id
  >;

  public readonly dica: HasManyThroughRepositoryFactory<LinceTip, typeof LinceTip.prototype.id,
    LinceTipUser,
    typeof LinceUser.prototype.id
  >;

  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource, @repository.getter('LinceUserProfileRepository') protected linceUserProfileRepositoryGetter: Getter<LinceUserProfileRepository>, @repository.getter('LinceProfileRepository') protected linceProfileRepositoryGetter: Getter<LinceProfileRepository>, @repository.getter('LinceTipUserRepository') protected linceTipUserRepositoryGetter: Getter<LinceTipUserRepository>, @repository.getter('LinceTipRepository') protected linceTipRepositoryGetter: Getter<LinceTipRepository>,
  ) {
    super(LinceUser, dataSource);
    this.dica = this.createHasManyThroughRepositoryFactoryFor('dica', linceTipRepositoryGetter, linceTipUserRepositoryGetter,);
    this.perfil = this.createHasManyThroughRepositoryFactoryFor('perfil', linceProfileRepositoryGetter, linceUserProfileRepositoryGetter,);
  }
}
