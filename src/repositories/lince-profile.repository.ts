import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, HasManyRepositoryFactory, HasManyThroughRepositoryFactory} from '@loopback/repository';
import {PostgreDataSource} from '../datasources';
import {LinceProfile, LinceProfileRelations, LinceTip, ExamClasses, LinceProfileExams} from '../models';
import {LinceTipRepository} from './lince-tip.repository';
import {LinceProfileExamsRepository} from './lince-profile-exams.repository';
import {ExamClassesRepository} from './exam-classes.repository';

export class LinceProfileRepository extends DefaultCrudRepository<
  LinceProfile,
  typeof LinceProfile.prototype.id,
  LinceProfileRelations
  > {

  public readonly dica: HasManyRepositoryFactory<LinceTip, typeof LinceProfile.prototype.id>;

  public readonly exame: HasManyThroughRepositoryFactory<ExamClasses, typeof ExamClasses.prototype.id,
          LinceProfileExams,
          typeof LinceProfile.prototype.id
        >;

  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource, @repository.getter('LinceTipRepository') protected linceTipRepositoryGetter: Getter<LinceTipRepository>, @repository.getter('LinceProfileExamsRepository') protected linceProfileExamsRepositoryGetter: Getter<LinceProfileExamsRepository>, @repository.getter('ExamClassesRepository') protected examClassesRepositoryGetter: Getter<ExamClassesRepository>,
  ) {
    super(LinceProfile, dataSource);
    this.exame = this.createHasManyThroughRepositoryFactoryFor('exame', examClassesRepositoryGetter, linceProfileExamsRepositoryGetter,);
    this.dica = this.createHasManyRepositoryFactoryFor('dica', linceTipRepositoryGetter,);
    this.registerInclusionResolver('dica', this.dica.inclusionResolver);
  }
}
