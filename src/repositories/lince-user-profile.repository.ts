import {DefaultCrudRepository} from '@loopback/repository';
import {LinceUserProfile, LinceUserProfileRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceUserProfileRepository extends DefaultCrudRepository<
  LinceUserProfile,
  typeof LinceUserProfile.prototype.id,
  LinceUserProfileRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LinceUserProfile, dataSource);
  }
}
