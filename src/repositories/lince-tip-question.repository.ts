import {DefaultCrudRepository, repository, HasManyRepositoryFactory} from '@loopback/repository';
import {LinceTipQuestion, LinceTipQuestionRelations, LinceTipUserAnswer} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {LinceTipUserAnswerRepository} from './lince-tip-user-answer.repository';

export class LinceTipQuestionRepository extends DefaultCrudRepository<
  LinceTipQuestion,
  typeof LinceTipQuestion.prototype.id,
  LinceTipQuestionRelations
> {

  public readonly usuarioResposta: HasManyRepositoryFactory<LinceTipUserAnswer, typeof LinceTipQuestion.prototype.id>;

  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource, @repository.getter('LinceTipUserAnswerRepository') protected linceTipUserAnswerRepositoryGetter: Getter<LinceTipUserAnswerRepository>,
  ) {
    super(LinceTipQuestion, dataSource);
    this.usuarioResposta = this.createHasManyRepositoryFactoryFor('usuarioResposta', linceTipUserAnswerRepositoryGetter,);
    this.registerInclusionResolver('usuarioResposta', this.usuarioResposta.inclusionResolver);
  }
}
