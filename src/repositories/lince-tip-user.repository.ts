import {DefaultCrudRepository, repository, BelongsToAccessor, HasOneRepositoryFactory} from '@loopback/repository';
import {LinceTipUser, LinceTipUserRelations, LinceUser, LinceTipUserAnswer} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject, Getter} from '@loopback/core';
import {LinceUserRepository} from './lince-user.repository';
import {LinceTipUserAnswerRepository} from './lince-tip-user-answer.repository';

export class LinceTipUserRepository extends DefaultCrudRepository<
  LinceTipUser,
  typeof LinceTipUser.prototype.id,
  LinceTipUserRelations
> {

  public readonly linceUsuario: BelongsToAccessor<LinceUser, typeof LinceTipUser.prototype.id>;

  public readonly usuarioResposta: HasOneRepositoryFactory<LinceTipUserAnswer, typeof LinceTipUser.prototype.id>;

  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource, @repository.getter('LinceUserRepository') protected linceUserRepositoryGetter: Getter<LinceUserRepository>, @repository.getter('LinceTipUserAnswerRepository') protected linceTipUserAnswerRepositoryGetter: Getter<LinceTipUserAnswerRepository>,
  ) {
    super(LinceTipUser, dataSource);
    this.usuarioResposta = this.createHasOneRepositoryFactoryFor('usuarioResposta', linceTipUserAnswerRepositoryGetter);
    this.registerInclusionResolver('usuarioResposta', this.usuarioResposta.inclusionResolver);
    this.linceUsuario = this.createBelongsToAccessorFor('linceUsuario', linceUserRepositoryGetter,);
    this.registerInclusionResolver('linceUsuario', this.linceUsuario.inclusionResolver);
  }
}
