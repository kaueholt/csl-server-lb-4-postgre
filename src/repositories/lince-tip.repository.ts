import {inject, Getter} from '@loopback/core';
import {DefaultCrudRepository, repository, BelongsToAccessor, HasManyRepositoryFactory} from '@loopback/repository';
import {PostgreDataSource} from '../datasources';
import {LinceTip, LinceTipRelations, LinceProfile, LinceTipQuestion, LinceTipUser} from '../models';
import {LinceProfileRepository} from './lince-profile.repository';
import {LinceTipQuestionRepository} from './lince-tip-question.repository';
import {LinceTipUserRepository} from './lince-tip-user.repository';

export class LinceTipRepository extends DefaultCrudRepository<
  LinceTip,
  typeof LinceTip.prototype.id,
  LinceTipRelations
  > {

  public readonly perfil: BelongsToAccessor<LinceProfile, typeof LinceTip.prototype.id>;

  public readonly dicaPergunta: HasManyRepositoryFactory<LinceTipQuestion, typeof LinceTip.prototype.id>;

  public readonly usuarioDica: HasManyRepositoryFactory<LinceTipUser, typeof LinceTip.prototype.id>;

  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource, @repository.getter('LinceProfileRepository') protected linceProfileRepositoryGetter: Getter<LinceProfileRepository>, @repository.getter('LinceTipQuestionRepository') protected linceTipQuestionRepositoryGetter: Getter<LinceTipQuestionRepository>, @repository.getter('LinceTipUserRepository') protected linceTipUserRepositoryGetter: Getter<LinceTipUserRepository>,
  ) {
    super(LinceTip, dataSource);
    this.usuarioDica = this.createHasManyRepositoryFactoryFor('usuarioDica', linceTipUserRepositoryGetter,);
    this.registerInclusionResolver('usuarioDica', this.usuarioDica.inclusionResolver);
    this.dicaPergunta = this.createHasManyRepositoryFactoryFor('dicaPergunta', linceTipQuestionRepositoryGetter,);
    this.registerInclusionResolver('dicaPergunta', this.dicaPergunta.inclusionResolver);
    this.perfil = this.createBelongsToAccessorFor('perfil', linceProfileRepositoryGetter,);
    this.registerInclusionResolver('perfil', this.perfil.inclusionResolver);
  }
}
