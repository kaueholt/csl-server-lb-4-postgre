import {DefaultCrudRepository} from '@loopback/repository';
import {EquipmentModels, EquipmentModelsRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class EquipmentModelsRepository extends DefaultCrudRepository<
  EquipmentModels,
  typeof EquipmentModels.prototype.id,
  EquipmentModelsRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(EquipmentModels, dataSource);
  }
}
