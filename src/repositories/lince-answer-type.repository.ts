import {DefaultCrudRepository} from '@loopback/repository';
import {LinceAnswerType, LinceAnswerTypeRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceAnswerTypeRepository extends DefaultCrudRepository<
  LinceAnswerType,
  typeof LinceAnswerType.prototype.id,
  LinceAnswerTypeRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LinceAnswerType, dataSource);
  }
}
