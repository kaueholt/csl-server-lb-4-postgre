import {DefaultCrudRepository} from '@loopback/repository';
import {LinceWebhook, LinceWebhookRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceWebhookRepository extends DefaultCrudRepository<
  LinceWebhook,
  typeof LinceWebhook.prototype.id,
  LinceWebhookRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LinceWebhook, dataSource);
  }
}
