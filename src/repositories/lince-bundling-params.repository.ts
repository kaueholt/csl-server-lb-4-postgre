import {DefaultCrudRepository} from '@loopback/repository';
import {LinceBundlingParams, LinceBundlingParamsRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceBundlingParamsRepository extends DefaultCrudRepository<
  LinceBundlingParams,
  typeof LinceBundlingParams.prototype.id,
  LinceBundlingParamsRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LinceBundlingParams, dataSource);
  }
}
