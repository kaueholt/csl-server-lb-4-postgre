import {DefaultCrudRepository} from '@loopback/repository';
import {ExamClasses, ExamClassesRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ExamClassesRepository extends DefaultCrudRepository<
  ExamClasses,
  typeof ExamClasses.prototype.id,
  ExamClassesRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(ExamClasses, dataSource);
  }
}
