import {DefaultCrudRepository} from '@loopback/repository';
import {LinceProfileExams, LinceProfileExamsRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceProfileExamsRepository extends DefaultCrudRepository<
  LinceProfileExams,
  typeof LinceProfileExams.prototype.id,
  LinceProfileExamsRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LinceProfileExams, dataSource);
  }
}
