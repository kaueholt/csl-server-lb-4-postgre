import {DefaultCrudRepository} from '@loopback/repository';
import {LincePartner, LincePartnerRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LincePartnerRepository extends DefaultCrudRepository<
  LincePartner,
  typeof LincePartner.prototype.id,
  LincePartnerRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LincePartner, dataSource);
  }
}
