import {DefaultCrudRepository} from '@loopback/repository';
import {LinceTipUserAnswer, LinceTipUserAnswerRelations} from '../models';
import {PostgreDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class LinceTipUserAnswerRepository extends DefaultCrudRepository<
  LinceTipUserAnswer,
  typeof LinceTipUserAnswer.prototype.id,
  LinceTipUserAnswerRelations
> {
  constructor(
    @inject('datasources.postgre') dataSource: PostgreDataSource,
  ) {
    super(LinceTipUserAnswer, dataSource);
  }
}
