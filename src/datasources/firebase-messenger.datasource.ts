import {inject, lifeCycleObserver, LifeCycleObserver} from '@loopback/core';
import {juggler} from '@loopback/repository';

const config = {
  name: 'firebaseMessenger',
  connector: 'rest',
  baseURL: 'fcm.googleapis.com/fcm/send',
  crud: false,
  options: {
    headers: {
      accept: 'application/json',
      'content-type': 'application/json',
      authorization: 'AAAAvAYebcM:APA91bGRiWMVsw2oObI4_8dkS_Vss-PtXZwyktFYRh1ONbq2pMMP2_jdb-FQBErULvEw7z_MVm6hYmsvbe2eOGGkbSxuABfiwufJxQ_M6UfGBYvDiThVSRSHrGtxGcTm6TSZUVfZv7z6 '
    },
  },
  // operations: [
  //   {
  //     template: {
  //       method: 'POST',
  //       url: 'https://swapi.co/api/people/{personId}',
  //     },
  //     functions: {
  //       getCharacter: ['personId'],
  //     },
  //   },
  // ]
};

// Observe application's life cycle to disconnect the datasource when
// application is stopped. This allows the application to be shut down
// gracefully. The `stop()` method is inherited from `juggler.DataSource`.
// Learn more at https://loopback.io/doc/en/lb4/Life-cycle.html
@lifeCycleObserver('datasource')
export class FirebaseMessengerDataSource extends juggler.DataSource
  implements LifeCycleObserver {
  static dataSourceName = 'firebaseMessenger';
  static readonly defaultConfig = config;

  constructor(
    @inject('datasources.config.firebaseMessenger', {optional: true})
    dsConfig: object = config,
  ) {
    super(dsConfig);
  }
}
