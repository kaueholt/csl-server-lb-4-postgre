import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    postgresql: {schema: 'public', table: 'lince_bundling_params'}
  }
})
export class LinceBundlingParams extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'endpointwebhook', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  endpointwebhook?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'username', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  username?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'password', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  password?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'token_login', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  tokenLogin?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'token_sessao', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  tokenSessao?: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'token_validade', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  tokenValidade?: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'parceiro_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  parceiroId: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceBundlingParams>) {
    super(data);
  }
}

export interface LinceBundlingParamsRelations {
  // describe navigational properties here
}

export type LinceBundlingParamsWithRelations = LinceBundlingParams & LinceBundlingParamsRelations;
