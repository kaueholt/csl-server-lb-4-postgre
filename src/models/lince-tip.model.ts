import {belongsTo, Entity, hasMany, model, property} from '@loopback/repository';
import {LinceProfile} from './lince-profile.model';
import {LinceTipQuestion} from './lince-tip-question.model';
import {LinceTipUser} from './lince-tip-user.model';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_tip'}}
})
export class LinceTip extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'titulo', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  titulo?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'descricao', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  descricao?: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'link', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  link?: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'status', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  status?: number;
  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  @property({
    type: 'string',
  })
  perfil_id?: string;

  @belongsTo(() => LinceProfile, {keyFrom: 'perfilId'}, {name: 'perfil_id'})
  perfilId: string;

  @hasMany(() => LinceTipQuestion, {keyTo: 'dicaId'})
  dicaPergunta: LinceTipQuestion[];

  @hasMany(() => LinceTipUser, {keyTo: 'dicaId'})
  usuarioDica: LinceTipUser[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceTip>) {
    super(data);
  }
}

export interface LinceTipRelations {
  // describe navigational properties here
}

export type LinceTipWithRelations = LinceTip & LinceTipRelations;
