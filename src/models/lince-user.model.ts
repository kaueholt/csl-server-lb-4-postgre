import {Entity, hasMany, model, property} from '@loopback/repository';
import {LinceProfile} from './lince-profile.model';
import {LinceTipUser} from './lince-tip-user.model';
import {LinceTip} from './lince-tip.model';
import {LinceUserProfile} from './lince-user-profile.model';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_user'}}
})
export class LinceUser extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'cpf', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  cpf?: number;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'status', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  status?: number;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'sesi_usuario_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  sesiUsuarioId: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  @hasMany(() => LinceProfile, {through: {model: () => LinceUserProfile, keyFrom: 'linceUsuarioId', keyTo: 'perfilId'}})
  perfil: LinceProfile[];

  @hasMany(() => LinceTip, {through: {model: () => LinceTipUser, keyFrom: 'linceUsuarioId', keyTo: 'dicaId'}})
  dica: LinceTip[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceUser>) {
    super(data);
  }
}

export interface LinceUserRelations {
  // describe navigational properties here
}

export type LinceUserWithRelations = LinceUser & LinceUserRelations;
