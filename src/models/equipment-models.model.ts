import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'equipment_models'}}
})
export class EquipmentModels extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    length: 30,
    postgresql: {columnName: 'name', dataType: 'character varying', dataLength: 30, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  name: string;

  @property({
    type: 'string',
    required: true,
    length: 30,
    postgresql: {columnName: 'maker', dataType: 'character varying', dataLength: 30, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  maker: string;

  @property({
    type: 'string',
    length: 200,
    postgresql: {columnName: 'description', dataType: 'character varying', dataLength: 200, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  description?: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<EquipmentModels>) {
    super(data);
  }
}

export interface EquipmentModelsRelations {
  // describe navigational properties here
}

export type EquipmentModelsWithRelations = EquipmentModels & EquipmentModelsRelations;
