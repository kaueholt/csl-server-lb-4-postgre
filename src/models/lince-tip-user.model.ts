import {belongsTo, Entity, model, property, hasOne} from '@loopback/repository';
import {LinceUser} from './lince-user.model';
import {LinceTipUserAnswer} from './lince-tip-user-answer.model';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_tip_user'}}
})
export class LinceTipUser extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'date',
    postgresql: {columnName: 'data_envio', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  dataEnvio?: string;

  @property({
    type: 'number',
    precision: 53,
    postgresql: {columnName: 'media', dataType: 'float', dataLength: null, dataPrecision: 53, dataScale: null, nullable: 'YES'},
  })
  media?: number;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;
  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'dica_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  dicaId: string;

  @belongsTo(() => LinceUser, {keyFrom: 'linceUsuarioId'}, {name: 'lince_usuario_id'})
  linceUsuarioId: string;

  @hasOne(() => LinceTipUserAnswer, {keyTo: 'usuarioDicaId'})
  usuarioResposta: LinceTipUserAnswer;
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceTipUser>) {
    super(data);
  }
}

export interface LinceTipUserRelations {
  // describe navigational properties here
}

export type LinceTipUserWithRelations = LinceTipUser & LinceTipUserRelations;
