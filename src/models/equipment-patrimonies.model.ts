import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    postgresql: {schema: 'public', table: 'equipment_patrimonies'}
  }
})
export class EquipmentPatrimonies extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 10,
    postgresql: {columnName: 'code', dataType: 'character varying', dataLength: 10, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  code?: string;

  @property({
    type: 'string',
    required: true,
    length: 32,
    postgresql: {columnName: 'mac', dataType: 'character varying', dataLength: 32, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  mac: string;

  @property({
    type: 'number',
    required: true,
    scale: 0,
    postgresql: {columnName: 'status', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'NO'},
  })
  status: number;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'token', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  token?: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'equipment_model_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  equipmentModelId?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<EquipmentPatrimonies>) {
    super(data);
  }
}

export interface EquipmentPatrimoniesRelations {
  // describe navigational properties here
}

export type EquipmentPatrimoniesWithRelations = EquipmentPatrimonies & EquipmentPatrimoniesRelations;
