import {Entity, model, property, hasMany} from '@loopback/repository';
import {LinceTip} from './lince-tip.model';
import {ExamClasses} from './exam-classes.model';
import {LinceProfileExams} from './lince-profile-exams.model';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_profile'}}
})
export class LinceProfile extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'descricao', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  descricao?: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  @hasMany(() => LinceTip, {keyTo: 'perfil_id'})
  dica: LinceTip[];

  @hasMany(() => ExamClasses, {through: {model: () => LinceProfileExams, keyFrom: 'perfilId', keyTo: 'sesiTipoExameId'}})
  exame: ExamClasses[];
  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceProfile>) {
    super(data);
  }
}

export interface LinceProfileRelations {
  // describe navigational properties here
}

export type LinceProfileWithRelations = LinceProfile & LinceProfileRelations;
