import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: false,
    postgresql: {schema: 'public', table: 'lince_hilab_webhook'}
  }
})
export class LinceHilabWebhook extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'parceiro_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  parceiroId: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'token', dataType: 'text', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  token?: string;

  @property({
    type: 'string',
    length: 80,
    postgresql: {columnName: 'type', dataType: 'character varying', dataLength: 80, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  type?: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'timestamp', dataType: 'bigint', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  timestamp?: number;

  @property({
    type: 'string',
    postgresql: {columnName: 'patient', dataType: 'json', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  patient?: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'custom', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  custom?: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'start_time', dataType: 'bigint', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  startTime?: number;

  @property({
    type: 'string',
    length: 20,
    postgresql: {columnName: 'CNPJ', dataType: 'character varying', dataLength: 20, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  CNPJ?: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceHilabWebhook>) {
    super(data);
  }
}

export interface LinceHilabWebhookRelations {
  // describe navigational properties here
}

export type LinceHilabWebhookWithRelations = LinceHilabWebhook & LinceHilabWebhookRelations;
