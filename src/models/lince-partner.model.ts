import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {idInjection: true, postgresql: {schema: 'public', table: 'lince_partner'}}
})
export class LincePartner extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'nome', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  nome?: string;

  @property({
    type: 'string',
    length: 30,
    postgresql: {columnName: 'CNPJ', dataType: 'character varying', dataLength: 30, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  cnpj?: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'parceiro_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  parceiroId?: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LincePartner>) {
    super(data);
  }
}

export interface LincePartnerRelations {
  // describe navigational properties here
}

export type LincePartnerWithRelations = LincePartner & LincePartnerRelations;
