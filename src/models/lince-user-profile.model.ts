import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    postgresql: {schema: 'public', table: 'lince_user_profile'}
  }
})
export class LinceUserProfile extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'number',
    scale: 0,
    postgresql: {columnName: 'status', dataType: 'integer', dataLength: null, dataPrecision: null, dataScale: 0, nullable: 'YES'},
  })
  status?: number;

  @property({
    type: 'string',
    length: 255,
    required: true,
    default: 'USUÁRIO',
    postgresql: {columnName: 'origem_cadastro', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  origemCadastro?: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'lince_usuario_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  linceUsuarioId: string;

  @property({
    type: 'string',
    postgresql: {columnName: 'perfil_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  perfilId?: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceUserProfile>) {
    super(data);
  }
}

export interface LinceUserProfileRelations {
  // describe navigational properties here
}

export type LinceUserProfileWithRelations = LinceUserProfile & LinceUserProfileRelations;
