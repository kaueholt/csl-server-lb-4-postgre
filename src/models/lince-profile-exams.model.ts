import {Entity, model, property} from '@loopback/repository';

@model({
  settings: {
    idInjection: true,
    postgresql: {schema: 'public', table: 'lince_profile_exams'}
  }
})
export class LinceProfileExams extends Entity {
  @property({
    type: 'string',
    defaultFn: 'uuid',
    required: false,
    id: 1,
    postgresql: {columnName: 'id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  id: string;

  @property({
    type: 'string',
    length: 255,
    postgresql: {columnName: 'questao', dataType: 'character varying', dataLength: 255, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  questao?: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'perfil_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  perfilId: string;

  @property({
    type: 'string',
    required: true,
    postgresql: {columnName: 'sesi_tipoexame_id', dataType: 'uuid', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'NO'},
  })
  sesiTipoExameId: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'created_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  createdAt: string;

  @property({
    type: 'date',
    required: false,
    default: new Date(new Date()).toISOString(),
    postgresql: {columnName: 'updated_at', dataType: 'timestamp with time zone', dataLength: null, dataPrecision: null, dataScale: null, nullable: 'YES'},
  })
  updatedAt: string;

  // Define well-known properties here

  // Indexer property to allow additional data
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  [prop: string]: any;

  constructor(data?: Partial<LinceProfileExams>) {
    super(data);
  }
}

export interface LinceProfileExamsRelations {
  // describe navigational properties here
}

export type LinceProfileExamsWithRelations = LinceProfileExams & LinceProfileExamsRelations;
